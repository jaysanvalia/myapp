package com.glb.service;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.glb.beans.Data;
import com.glb.beans.DataV;
import com.glb.beans.SearchData;
import com.glb.dao.DataDAO;

@Service
public class DataServiceImpl implements DataService {

	@Autowired
	DataDAO dataDAO;

	@Override
	public List<DataV> getData(SearchData searchData) {

		return setDataV(dataDAO.getData(searchData));
	}

	private List<DataV> setDataV(List<Data> data) {

		return data.stream().map(new Function<Data, DataV>() {

			@Override
			public DataV apply(Data t) {
				DataV dataV = new DataV();
				dataV.setAmount(t.getAmount());
				dataV.setLastname(t.getLastname());
				dataV.setId(t.getId());
				dataV.setMobileNumber(t.getMobileNumber());
				dataV.setName(t.getName());
				return dataV;
			}
		}).collect(Collectors.toCollection(LinkedList::new));
	}
}