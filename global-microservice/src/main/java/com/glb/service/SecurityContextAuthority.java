package com.glb.service;

import org.springframework.security.core.GrantedAuthority;

public enum SecurityContextAuthority implements GrantedAuthority {

    ROLE_ADMIN, ROLE_USER;

    @Override
    public String getAuthority() {
        return name();
    }
}