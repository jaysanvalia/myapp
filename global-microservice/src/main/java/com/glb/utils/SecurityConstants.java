package com.glb.utils;
public class SecurityConstants {
    public static final String SECRET = "SecretKeyToGenJWTs";
    public static final long EXPIRATION_TIME = 12000_000; // 10 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String USER = "USER";
    public static final String SIGN_UP_URL = "/users/sign-up";
    public static final String ROLE_PREFIX = "ROLE_";
    public static Long TOTAL_RECORDS;
}