import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { User } from '../models/user';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { SearchData } from '../models/SearchData';

@Injectable()
export class GlobalDataService {
 public BASE_URL: string = 'http://localhost:8080';
  constructor(private router: Router, private auth: AuthService,private http: Http) { }
  
   getDatas(searchData: SearchData): Promise<any> {
   alert(this.BASE_URL)
    let url: string = `${this.BASE_URL}/rest/getData`;
    return this.http.post(url, searchData, {headers: this.auth.headers}).toPromise();
  }

}
