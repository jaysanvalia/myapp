package com.glb;
import org.springframework.batch.core.configuration.annotation.SimpleBatchConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.batch.BatchAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.glb.config.BatchConfiguration;
import com.glb.config.DBConfiguration;
import com.glb.controller.SpringBootAppWS;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;

@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages="com.glb",exclude = { DataSourceAutoConfiguration.class ,BatchAutoConfiguration.class})
@Import(value={ DBConfiguration.class,BatchConfiguration.class,WebSecurity.class,SpringBootAppWS.class})
public class WebConfig extends SpringBootServletInitializer {


    /**
     * Run the spring boot application in embedded tomcat 
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        SpringApplication.run(WebConfig.class, args);
    }
    
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(WebConfig.class);
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
    @Bean
	public Cache cacheManager() {
		return CacheManager.newInstance().getCache("token");
	}
    
   /* @Bean
    public TestRestTemplate restTemplate() {
        return new TestRestTemplate();
    }*/

}