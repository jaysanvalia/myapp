package com.glb.service;

import net.sf.ehcache.Element;

public interface TokenSericeCache {
	public Element checkUserToken(String user);
	public boolean addUserToken(String user,String token);
	public boolean deleteUserToken(String user);
}
