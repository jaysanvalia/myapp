package com.glb.beans;

public class GenericV<T> {
	
	Integer page;
	Long totalRec;
	T objects;
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public Long getTotalRec() {
		return totalRec;
	}
	public void setTotalRec(Long totalRec) {
		this.totalRec = totalRec;
	}
	public T getObjects() {
		return objects;
	}
	public void setObjects(T objects) {
		this.objects = objects;
	}
	
	

}
