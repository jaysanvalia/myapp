package com.glb.config;
import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
 
@Configuration
@EnableTransactionManagement
public class DBConfiguration {
    @Value("${db.driver}")
    private String DRIVER;
 
    @Value("${db.password}")
    private String PASSWORD;
 
    @Value("${db.url}")
    private String URL;
 
    @Value("${db.username}")
    private String USERNAME;
 
    @Value("${hibernate.dialect}")
    private String DIALECT;
 
    @Value("${hibernate.show_sql}")
    private String SHOW_SQL;
 
    @Value("${entitymanager.packagesToScan}")
    private String PACKAGES_TO_SCAN;
    
    @Value("${hibernate.hbm2ddl.auto}")
    private String hibernate_hbm2ddl_auto;

    @Value("${hibernate.connection.provider_disables_autocommit}")
	private String disablesAutocommit;
    
    @Value("${hibernate.jdbc.batch_size}")
   	private String a;
    
    @Value("${cache.provider_class}")
   	private String b;
    
    @Value("${hibernate.cache.use_second_level_cach}")
   	private String c;
 
    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(DRIVER);
        dataSource.setUrl(URL);
        dataSource.setUsername(USERNAME);
        dataSource.setPassword(PASSWORD);
        return dataSource;
    }
 
    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan(PACKAGES_TO_SCAN);
        sessionFactory.setHibernateProperties(setProperties());
        return sessionFactory;
    }

	private Properties setProperties() {
		Properties hibernateProperties = new Properties();
        hibernateProperties.put("hibernate.dialect", DIALECT);
        hibernateProperties.put("hibernate.show_sql", SHOW_SQL);
        //hibernateProperties.put("hibernate.hbm2ddl.auto", hibernate_hbm2ddl_auto);
        hibernateProperties.put("hibernate.connection.provider_disables_autocommit", disablesAutocommit);
        hibernateProperties.put("hibernate.cache.use_second_level_cach", c);
        hibernateProperties.put("cache.provider_class", b);
        hibernateProperties.put("hibernate.jdbc.batch_size", a);
		return hibernateProperties;
	}
 
   @Bean
    public HibernateTransactionManager transactionManager() {
        HibernateTransactionManager transactionManager = new HibernateTransactionManager();
        transactionManager.setSessionFactory(sessionFactory().getObject());
        return transactionManager;
    }
}