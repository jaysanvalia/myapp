package com.glb.beans;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;


/**
 * The persistent class for the FILEDATA database table.
 * 
 */
@Entity
@Table(name="FILEDATA")
@NamedQuery(name="Filedata.findAll", query="SELECT f FROM Filedata f")
public class Filedata implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(unique=true, nullable=false, precision=10)
	private Integer fileId;

	@Column(nullable=false, length=20)
	private String filename;

	//bi-directional many-to-one association to Data
	@OneToMany(mappedBy="filedata",fetch=FetchType.LAZY)
	@Cascade(value={
			CascadeType.PERSIST,CascadeType.DELETE,CascadeType.DELETE_ORPHAN,CascadeType.SAVE_UPDATE
	})
	@NotNull
	private Set<Data> data;

	public Filedata() {
	}


	public Integer getFileId() {
		return fileId;
	}


	public void setFileId(Integer fileId) {
		this.fileId = fileId;
	}


	public String getFilename() {
		return this.filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public Set<Data> getData() {
		return this.data;
	}

	/*public void setData(Set<Data> data) {
		this.data = data;
	}

	public Data addData(Data data) {
		getData().add(data);
		data.setFiledata(this);

		return data;
	}

	public Data removeData(Data data) {
		getData().remove(data);
		data.setFiledata(null);

		return data;
	}*/


	@Override
	public String toString() {
		return "Filedata [fileId=" + fileId + ", filename=" + filename
				+ ", data=" + data + "]";
	}
	

}