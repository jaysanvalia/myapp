import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { User } from '../../models/user';

@Component({
  selector: 'logins',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
	errorMessage: String ="";
  user: User = new User();
  constructor(private router: Router, private auth: AuthService) {}
  onLogin(): void {
    this.auth.login(this.user)
    .then((res: Response) => {
	this.auth.userHeader=res.headers.get('USER');
	console.log("aadhahdad::"+res.headers.get('USER'));
      this.router.navigateByUrl('/main');
    })
    .catch((err) => {
      console.log(err.json());
	 this.errorMessage =  err.json().message;
    });
  }
}