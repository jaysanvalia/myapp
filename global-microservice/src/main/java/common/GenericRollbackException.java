package common;

public class GenericRollbackException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GenericRollbackException(Throwable throwable) {
		super(throwable);
	}
	
	@Override
	public String toString() {
		return super.toString();
	}
	
}
