package com.glb.service;

import java.util.List;

import com.glb.beans.DataV;
import com.glb.beans.SearchData;
import com.glb.utils.GlobalException;

public interface DataService {
	List<DataV> getData(SearchData searchData) throws GlobalException;
}
