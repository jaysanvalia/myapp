package common;

import java.util.List;

import org.hibernate.Session;

public interface GenericCRUDDAO<I,O> {

	Session getSession();

	void save(List<I> i);

	Integer save(I i);

	void update(List<I> i);

	void update(I i);

	void saveUpdate(List<I> i);

	void saveUpdate(I i);

	void delete(I i);

	O merge(I i);
}
