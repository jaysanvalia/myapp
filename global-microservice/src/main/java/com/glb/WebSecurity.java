package com.glb;

import static com.glb.utils.SecurityConstants.SIGN_UP_URL;

import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.glb.filter.JWTAuthenticationFilter;
import com.glb.filter.JWTAuthorizationFilter;
import com.glb.filter.SimpleCORSFilter;
import com.glb.service.TokenSericeCache;

@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {
	private UserDetailsService userDetailsService;
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	private TokenSericeCache tokenSericeCache;

	public WebSecurity(UserDetailsService userDetailsService,
			BCryptPasswordEncoder bCryptPasswordEncoder,TokenSericeCache tokenSericeCache) {
		this.userDetailsService = userDetailsService;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.tokenSericeCache=tokenSericeCache;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf()
				.disable()
				.authorizeRequests()
				.antMatchers("/rest/homePage").access("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
				.antMatchers("/rest/userPage").access("hasRole('ROLE_USER')")
				.antMatchers("/rest/adminPage").access("hasRole('ROLE_ADMIN')")
				.antMatchers(HttpMethod.POST, SIGN_UP_URL).permitAll()
				.and()
				.formLogin()
				.loginPage("/login")
				.defaultSuccessUrl("/homePage")
				.failureUrl("/loginPage?error")
				.and()
				.logout()
				.logoutSuccessUrl("/loginPage?logout")
				.and()
				.addFilterBefore(new SimpleCORSFilter(), JWTAuthenticationFilter.class)
				.addFilter(new JWTAuthenticationFilter(authenticationManager(),userDetailsService,tokenSericeCache))
				.addFilter(new JWTAuthorizationFilter(authenticationManager(),userDetailsService,tokenSericeCache));
	}

	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(
				bCryptPasswordEncoder);
	}
}