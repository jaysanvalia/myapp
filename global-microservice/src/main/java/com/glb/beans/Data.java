package com.glb.beans;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


/**
 * The persistent class for the "DATA" database table.
 * 
 */
@Entity
@Table(name="DATA")
@NamedQuery(name="Data.findAll", query="SELECT d FROM Data d")
public class Data implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name="auto",strategy="increment")
	@GeneratedValue(generator="auto")
	@Column(unique=true, nullable=false, precision=10)
	private Integer id;

	@Column(length=40)
	private Integer amount;

	@Column(length=40)
	private String description;

	@Column(name="EMAIL_ID", length=40)
	private String emailId;

	@Column(length=40)
	private String lastname;

	@Column(name="MOBILE_NUMBER", length=40)
	private String mobileNumber;

	@Column(length=40)
	private String name;
	
	//bi-directional many-to-one association to Filedata
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="FILENAME_ID")
	private Filedata filedata;

	

	public Data() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAmount() {
		return this.amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEmailId() {
		return this.emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getMobileNumber() {
		return this.mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Filedata getFiledata() {
		return this.filedata;
	}

	public void setFiledata(Filedata filedata) {
		this.filedata = filedata;
	}

	@Override
	public String toString() {
		return "Data [id=" + id + ", amount=" + amount + ", description="
				+ description + ", emailId=" + emailId + ", lastname="
				+ lastname + ", mobileNumber=" + mobileNumber + ", name="
				+ name + ", filedata=" + filedata + "]";
	}

}