package common;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(isolation=Isolation.DEFAULT,propagation=Propagation.REQUIRED, rollbackFor=GenericRollbackException.class)
public class GenericCRUDDAOImpl<I,O> implements GenericCRUDDAO<I,O> {

	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public Session getSession(){
		return sessionFactory.getCurrentSession();
	}
	@Override
	public void save(List<I> i) throws GenericRollbackException {
		 getSession().save(i);
	}
	@Override
	public Integer save(I i)throws GenericRollbackException {
		return (Integer) getSession().save(i);
		
	}
	@Override
	public void update(List<I> i)throws GenericRollbackException {
		getSession().update(i);
		
	}
	@Override
	public void update(I i)throws GenericRollbackException {
		getSession().update(i);
		
	}
	@Override
	public void saveUpdate(List<I> i)throws GenericRollbackException {
		getSession().saveOrUpdate(i);
	}
	@Override
	public void saveUpdate(I i)throws GenericRollbackException {
		getSession().saveOrUpdate(i);
	}
	@Override
	public void delete(I i)throws GenericRollbackException {
		getSession().delete(i);
	}
	@Override
	public O merge(I i)throws GenericRollbackException {
		return (O) getSession().merge(i);
	}
}
