import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { User } from '../models/user';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class AuthService {
  public BASE_URL: string = 'http://localhost:8080';
  public headers: Headers = new Headers({'Content-Type': 'application/json'});
   public userHeader: String="";
  constructor(private http: Http) {}
  login(user: User): Promise<any> {
    let url: string = `${this.BASE_URL}/login`;
	
    return this.http.post(url, user,{headers: this.headers}).toPromise();
  }
  register(user: User): Promise<any> {
    let url: string = '${this.BASE_URL}/users/sign-up';
    return this.http.post(url, user, {headers: this.headers}).toPromise();
  }
  
  ensureAuthenticated(user): Promise<any> {
  let url: string = `${this.BASE_URL}/rest/status`;
  let headers: Headers = new Headers({
    'Content-Type': 'application/json',
    'USER': user
  });
  return this.http.post(url,user, {headers: headers}).toPromise();
}
}