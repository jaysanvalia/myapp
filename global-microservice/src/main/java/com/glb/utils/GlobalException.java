package com.glb.utils;

public class GlobalException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public GlobalException(Throwable throwable) {
		super(throwable);
	}
	
	@Override
	public String toString() {
		return super.toString();
	}
}
