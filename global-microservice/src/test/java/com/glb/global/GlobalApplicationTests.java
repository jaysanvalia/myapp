package com.glb.global;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.glb.WebConfig;
import com.glb.beans.GenericV;
import com.glb.beans.SearchData;

@RunWith(SpringRunner.class)
@SpringBootTest(classes={WebConfig.class})
public class GlobalApplicationTests {

	@Autowired
	private TestRestTemplate restTemplate;

	SearchData searchData;

	@Before
	public void init() {
		searchData=new SearchData();
		searchData.setFileId("1000");
		searchData.setPageNumber(0);
	}

	@Test
	public void testCreateClientSuccessfully() throws Exception {
		// given(dataServiceMock.getData(searchData).get(0)).willReturn(new
		// DataV());
		/*
		 * mockMvc.perform( post("/clients")
		 * .contentType(MediaType.APPLICATION_JSON) .content( objectMapper
		 * .writeValueAsBytes(new CreateClientRequest( "Foo"))))
		 * .andExpect(status().isCreated()) .andExpect(jsonPath("$.name",
		 * is("Foo"))) .andExpect(jsonPath("$.number", notNullValue()));
		 */

		ResponseEntity<GenericV> responseEntity = restTemplate.postForEntity(
				"http://localhost:8080/getData", searchData, GenericV.class);
		GenericV<List> client = responseEntity.getBody();
		assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
		assertEquals(3, client.getObjects().size());
	}
}
