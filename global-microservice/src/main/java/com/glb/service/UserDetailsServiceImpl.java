package com.glb.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.glb.beans.AccountCredentials;
import com.glb.dao.UserDAO;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	private UserDAO userDAO;

	public UserDetailsServiceImpl(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		AccountCredentials applicationUser = userDAO.getUser(username);
		if (applicationUser == null) {
			throw new UsernameNotFoundException(username);
		}
		List a = new ArrayList();
		String[] roles = applicationUser.getRole().split(",");
		for (String role : roles) {
			if(role.equals("USER"))
				a.add(SecurityContextAuthority.ROLE_USER);
			else
				a.add(SecurityContextAuthority.ROLE_ADMIN);
		}
		return new User(applicationUser.getUsername(),
				applicationUser.getPassword(), a);
	}
	
	
}