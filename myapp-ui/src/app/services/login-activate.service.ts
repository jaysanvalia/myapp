import { Injectable }       from '@angular/core';
import { CanActivate, CanActivateChild, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../services/auth.service';


@Injectable()
export class LoginActivateService implements CanActivate, CanActivateChild {
  	errorMessage: String ="";
  user: String = "";
  constructor(private authService: AuthService, private router: Router) {
  }
  canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        let url: string = state.url;
	console.log('Url:'+ url);
	
	let status=this.ensureAuthenticated(this.authService.userHeader)
			console.log("status::"+status);
      		if (status) {
				console.log("/main");
				return false;
			}
	console.log("/login");
	return true;	
  }
  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
      //  let loggedInUser= this.authService.getLoggedInUser();
	//if (loggedInUser.role === 'ADMIN') {
	//        return true;		
	//} else {
	//	console.log('Unauthorized to open link: '+ state.url);
	//	return false;
	//}
	
	return true;
	}  
	
	ensureAuthenticated(user): any {
    this.authService.ensureAuthenticated(user)
    .then((res: Response) => {
	
	console.log("aadhahdad::"+res);
      return res
    })
    .catch((err) => {
      console.log(err.json());
	 this.errorMessage =  err.json().message;
	 return err
    });
  }
} 