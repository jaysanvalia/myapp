package com.glb.utils;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.FORBIDDEN, reason="Token Not Found or Expired") //404
public class TokenExpired extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public TokenExpired(Throwable throwable) {
		super(throwable);
	}
	
	public TokenExpired(String string) {
		super(string);
	}

	public TokenExpired() {
		super();
	}

	@Override
	public String toString() {
		return super.toString();
	}
}