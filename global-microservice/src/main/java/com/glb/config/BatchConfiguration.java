package com.glb.config;

import java.util.Date;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.HibernateItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.FileSystemResource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.glb.beans.Data;
import com.glb.processor.CustomSkipListener;
import com.glb.processor.JobCompletionNotificationListener;
import com.glb.processor.OrderItemProcessor;

@Configuration
@EnableBatchProcessing
@EnableScheduling
@Import({ BatchScheduler.class })
public class BatchConfiguration {

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private SimpleJobLauncher jobLauncher;

	@Autowired
	public JobBuilderFactory jobBuilderFactory;

	@Autowired
	public StepBuilderFactory stepBuilderFactory;

	@Autowired
	CustomSkipListener customSkipListener;
	
	//@Scheduled(cron = "0 0/2 * * * ?")
	@Scheduled(cron = "0 20 * * * ?")
	public void perform() throws Exception {

		System.out.println("Job Started at :" + new Date());

		JobParameters param = new JobParametersBuilder().addString("JobID",
				String.valueOf(System.currentTimeMillis())).toJobParameters();

		JobExecution execution = jobLauncher.run(processOrderJob(), param);

		System.out
				.println("Job finished with status :" + execution.getStatus());
	}

	@Bean
	@Transactional
	public Job processOrderJob() {
		/*return jobBuilderFactory.get("processOrderJob")
				.incrementer(new RunIdIncrementer()).listener(listener())
				.start(orderStep()).on("").to(orderStep()).on("FAILED")
				.to(orderStep()).end().build();*/
		/*
		 * .flow(orderStep()) .end() .build();
		 */
		
		return jobBuilderFactory.get("processOrderJob")
				.incrementer(new RunIdIncrementer()).listener(listener())
				.start(orderStep(customSkipListener)).build();
	}

	@SuppressWarnings("unchecked")
	@Bean
	public Step orderStep(CustomSkipListener customSkipListener) {
		return stepBuilderFactory
				.get("orderStep")
				.<Data, Data> chunk(10)
				.reader(reader())
				.processor(
						(ItemProcessor<? super Data, ? extends Data>) processor())
				.writer(writer())
				.faultTolerant()
	            .skipLimit(10)
	            .skip(Exception.class)
	            .listener(customSkipListener).build();
	}

	@Bean
	public FlatFileItemReader<Data> reader() {
		FlatFileItemReader<Data> reader = new FlatFileItemReader<Data>();
		reader.setResource(new FileSystemResource("D:\\Patil.csv"));
		reader.setLineMapper(new DefaultLineMapper<Data>() {
			{
				setLineTokenizer(new DelimitedLineTokenizer(",") {
					{
						setNames(new String[] { "amount", "description" , "emailId", "lastname" , "mobileNumber", "name" });
					}
				});
				setFieldSetMapper(new BeanWrapperFieldSetMapper<Data>() {
					{
						setTargetType(Data.class);
					}
				});
			}
		});
		return reader;
	}

	@Bean
	public OrderItemProcessor processor() {
		return new OrderItemProcessor();
	}

	@SuppressWarnings("rawtypes")
	@Bean
	public HibernateItemWriter writer() {
		HibernateItemWriter<Data> writer = new HibernateItemWriter<>();
		writer.setSessionFactory(sessionFactory);
		return writer;
	}

	@Bean
	public JobExecutionListener listener() {
		return (JobExecutionListener) new JobCompletionNotificationListener();
	}

	/*
	 * @Bean public Job job() { return jobBuilderFactory.get("job")
	 * .incrementer(new RunIdIncrementer()) .start(step1()).next(step2())
	 * .build(); }
	 * 
	 * @Bean public Step step1() { return stepBuilderFactory.get("step1")
	 * .<String, String> chunk(1) .reader(new Reader()) .writer(new Writer())
	 * .build(); }
	 * 
	 * @Bean public Step step2() { return stepBuilderFactory.get("step2")
	 * .tasklet(taskletStep) .build(); }
	 */

}