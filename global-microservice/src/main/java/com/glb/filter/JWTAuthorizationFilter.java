package com.glb.filter;

import static com.glb.utils.SecurityConstants.SECRET;
import static com.glb.utils.SecurityConstants.TOKEN_PREFIX;
import static com.glb.utils.SecurityConstants.USER;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import com.glb.service.SecurityContextAuthority;
import com.glb.service.TokenSericeCache;
import com.glb.utils.TokenExpired;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import net.sf.ehcache.Element;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {
	UserDetailsService userDetailsService;
	TokenSericeCache tokenSericeCache;

	public JWTAuthorizationFilter(AuthenticationManager authManager,
			UserDetailsService userDetailsService) {
		super(authManager);
		this.userDetailsService = userDetailsService;
	}

	public JWTAuthorizationFilter(AuthenticationManager authManager,
			UserDetailsService userDetailsService,
			TokenSericeCache tokenSericeCache) {
		this(authManager, userDetailsService);
		this.tokenSericeCache = tokenSericeCache;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest req,
			HttpServletResponse res, FilterChain chain) throws IOException,
			ServletException {
		UsernamePasswordAuthenticationToken authentication;
		String user = req.getHeader(USER);
		System.out.println("user::"+user);
		if (user == null) {
			chain.doFilter(req, res);
			return;
		}
		Element element = tokenSericeCache.checkUserToken(user);
		System.out.println(element);
		if (element == null || element.isExpired()) {
			throw new TokenExpired("Access Forbiden");
		}
		try {
			authentication = getAuthentication(req, element);
		} catch (ExpiredJwtException e) {
			tokenSericeCache.deleteUserToken(user);
			throw new TokenExpired("Token Expired");
		}
		SecurityContextHolder.getContext().setAuthentication(authentication);
		chain.doFilter(req, res);
	}

	private UsernamePasswordAuthenticationToken getAuthentication(
			HttpServletRequest request, Element element) {
		String token = (String) element.getObjectValue();
		String userAndRoles = Jwts.parser().setSigningKey(SECRET)
				.parseClaimsJws(token.replace(TOKEN_PREFIX, "")).getBody()
				.getSubject();
		if (userAndRoles != null) {
			// parse the token.
			String roless = userAndRoles.split(",")[1];
			String user = userAndRoles.split(",")[0];

			List<SecurityContextAuthority> a = new ArrayList<>();
			String[] roles = roless.split(",");
			for (String role : roles) {
				if (role.equals("USER"))
					a.add(SecurityContextAuthority.ROLE_USER);
				else
					a.add(SecurityContextAuthority.ROLE_ADMIN);
			}
			System.out.println(user + "::::" + user + "::::::" + token
					+ "::::::::" + roles[0]);
			// a.stream().forEach(SecurityContextAuthority::getAuthority);
			if (user != null) {
				return new UsernamePasswordAuthenticationToken(user, null, a);
			}
			return null;
		}
		return null;
	}
}