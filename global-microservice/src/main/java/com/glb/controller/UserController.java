package com.glb.controller;
import java.util.ArrayList;
import java.util.List;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.glb.beans.AccountCredentials;
import com.glb.dao.UserDAO;
@RestController
@RequestMapping("/users")
public class UserController {
    private UserDAO userDAO;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    public UserController(UserDAO userDAO,
                          BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userDAO = userDAO;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }
    
    public void signUp(@RequestBody AccountCredentials user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userDAO.save(user);
    }
    
    @PostMapping("/sign-up")
    public List<String> signUp2(@RequestBody AccountCredentials user) {
    	AccountCredentials user2 = userDAO.getUser(user.getUsername());
    	List<String> a=new ArrayList<>();
		if(user2==null)
    	{
    		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
    		userDAO.save(user);
        	a.add("User Added");
        	return a;
    	}
		a.add("User Already Present");
		return a;
    }
    
    
}