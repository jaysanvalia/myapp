package com.glb.dao;

import java.util.List;

import com.glb.beans.Data;
import com.glb.beans.SearchData;
import com.glb.utils.GlobalException;

public interface DataDAO {

	List<Data> getData(SearchData searchData) throws GlobalException;
}
