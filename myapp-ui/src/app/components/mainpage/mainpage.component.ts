import { Component, OnInit } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { SearchData } from '../../models/SearchData';
import { GlobalDataService } from '../../services/global-data.service';
import { AuthService } from '../../services/auth.service';
import { DataRecords } from '../../models/data-records';
import { LoginActivateService } from '../../services/login-activate.service';

@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.css']
})
export class MainpageComponent implements OnInit {
	searchData: SearchData = new SearchData();
	//datasList: Array<DataRecords> = new Array<DataRecords>();
	datasList: any = [];
	errorMessage: String ="";
	page: number = 1;
	totalRec: number=0;
  constructor(private loginActivateService: LoginActivateService, private router: Router, location: PlatformLocation, private globalDataService: GlobalDataService, private auth: AuthService) {
     location.onPopState(() => {
			console.log("you have pressed back button");
			let status=this.loginActivateService.ensureAuthenticated(this.auth.userHeader)
			console.log("status::"+status);
      		if (status) {
				console.log("/main");
				this.router.navigateByUrl('/main');
			}
			console.log("/login");
			this.router.navigateByUrl('/login');
     }); 
	}
	
  ngOnInit() {
  }
  searchDatass(pageN): void {
  if(pageN ==null){
		pageN=this.page;
  }
   this.searchData.pageNumber = pageN;
   console.log(this.searchData.pageNumber);
	this.globalDataService.getDatas(this.searchData)
    .then((res: Response) => {
	console.log(Array.of(res.json()));
	//this.datasList = Array.of(res.json());
	this.datasList = res.json()["objects"];
	this.totalRec=res.json()["totalRec"];
	console.log(this.datasList);
	this.auth.userHeader=res.headers.get('USER');
	console.log("aadhahdad::"+res.headers.get('USER'));
      //this.router.navigateByUrl('/main');
    })
    .catch((err) => {
      console.log(err.json());
	 this.errorMessage =  err.json().message;
    });
	return pageN;
}
}
