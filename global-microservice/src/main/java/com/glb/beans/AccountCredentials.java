package com.glb.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="USERS")
public class AccountCredentials{

	@Id
	@SequenceGenerator(sequenceName="DATA_SEQ", name = "DATA_SEQ",allocationSize=0,initialValue=0)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DATA_SEQ")
	private Integer id;
	
	@Column(name="NAME")
	private String username;
	
	@Column(name="PASSWORD")
	private String password;
	
	String role;

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public AccountCredentials() {
		super();
	}

	public AccountCredentials(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "AccountCredentials [id=" + id + ", username=" + username
				+ ", password=" + password + ", role=" + role + "]";
	}

}