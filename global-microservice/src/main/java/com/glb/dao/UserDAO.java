package com.glb.dao;

import com.glb.beans.AccountCredentials;
import com.glb.utils.GlobalException;

import common.GenericCRUDDAO;

public interface UserDAO extends GenericCRUDDAO<AccountCredentials,AccountCredentials> {
	AccountCredentials getUser(String userName) throws GlobalException;
}
