package com.glb.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.sf.ehcache.Element;

@Component
public class TokenSericeCacheImpl implements TokenSericeCache{

	@Autowired
	TokenService tokenService;
	
	@Override
	public Element checkUserToken(String user) {
		Element elemnet=tokenService.getCacheUser(user);
		return elemnet;
	}
	
	@Override
	public boolean addUserToken(String user,String token){
		return tokenService.cacheUser(user, token)? true:false;
	}
	
	@Override
	public boolean deleteUserToken(String user){
		return tokenService.clearCache(user);
	}
	
	

}
