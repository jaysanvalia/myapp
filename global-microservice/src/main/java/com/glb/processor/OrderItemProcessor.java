package com.glb.processor;

import org.springframework.batch.item.ItemProcessor;

import com.glb.beans.Data;
import com.glb.beans.Filedata;

public class OrderItemProcessor implements ItemProcessor<Data, Data> {
    
    @Override
    public Data process(final Data data) throws Exception {
      
    	Filedata filedata=new Filedata();
    	filedata.setFileId(1);
    	filedata.setFilename("Patil.csv");
        data.setFiledata(filedata);
        System.out.println("Converting (" + filedata + ") to (" + data + ")");

        return data;
    }

}