package com.glb.filter;

import static com.glb.utils.SecurityConstants.EXPIRATION_TIME;
import static com.glb.utils.SecurityConstants.SECRET;
import static com.glb.utils.SecurityConstants.TOKEN_PREFIX;
import static com.glb.utils.SecurityConstants.USER;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.glb.beans.AccountCredentials;
import com.glb.service.TokenSericeCache;
import com.glb.utils.TokenExpired;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import net.sf.ehcache.Element;

public class JWTAuthenticationFilter extends
		UsernamePasswordAuthenticationFilter {
	private AuthenticationManager authenticationManager;
	private UserDetailsService userDetailsService;
	TokenSericeCache tokenSericeCache;

	public JWTAuthenticationFilter(AuthenticationManager authenticationManager,
			UserDetailsService userDetailsService) {
		this.authenticationManager = authenticationManager;
		this.userDetailsService = userDetailsService;
	}

	public JWTAuthenticationFilter(AuthenticationManager authenticationManager,
			UserDetailsService userDetailsService,
			TokenSericeCache tokenSericeCache) {
		this(authenticationManager, userDetailsService);
		this.tokenSericeCache = tokenSericeCache;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest req,
			HttpServletResponse res) throws AuthenticationException {
		try {
			AccountCredentials creds = new ObjectMapper().readValue(
					req.getInputStream(), AccountCredentials.class);
			System.out.println(creds);
			UserDetails userDetails = userDetailsService
					.loadUserByUsername(creds.getUsername());
			System.out.println(userDetails);
			/*
			 * String[] roles = userDetails.get.split(","); for (String role :
			 * roles) { if(role.equals("USER"))
			 * a.add(SecurityContextAuthority.ROLE_USER); else
			 * a.add(SecurityContextAuthority.ROLE_ADMIN); }
			 */
			return authenticationManager
					.authenticate(new UsernamePasswordAuthenticationToken(creds
							.getUsername(), creds.getPassword(), userDetails
							.getAuthorities()));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest req,
			HttpServletResponse res, FilterChain chain, Authentication auth)
			throws IOException, ServletException {
		Element element = tokenSericeCache.checkUserToken(((User) auth
				.getPrincipal()).getUsername());
		String token;
		System.out.println(((User) auth.getPrincipal()).getUsername());
		if (element != null && element.isExpired()) {
			throw new TokenExpired("Token for User " + element.getObjectKey()
					+ " has Expired");
		} else if (element == null) {
			System.out.println(element);
			Collection<GrantedAuthority> a = ((User) auth.getPrincipal())
					.getAuthorities();
			String aa = "";
			for (GrantedAuthority grantedAuthority : a) {
				aa = aa + grantedAuthority;
			}
			token = TOKEN_PREFIX
					+ Jwts.builder()
							.setSubject(
									((User) auth.getPrincipal()).getUsername()
											+ "," + aa)
							.setExpiration(
									new Date(System.currentTimeMillis()
											+ EXPIRATION_TIME))
							.signWith(SignatureAlgorithm.HS512, SECRET)
							.compact();
			tokenSericeCache.addUserToken(
					((User) auth.getPrincipal()).getUsername(), token);
			System.out.println(((User) auth.getPrincipal()).getUsername()
					+ "::::::" + token);
		} else {
			token = (String) element.getObjectValue();
			System.out.println(element + "::::" + element.isExpired()
					+ "::::::" + token);
		}
		res.setHeader(USER,  ((User) auth.getPrincipal()).getUsername());
	}
}