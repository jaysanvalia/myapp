package com.glb.controller;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.glb.beans.Data;
import com.glb.beans.DataV;
import com.glb.beans.GenericV;
import com.glb.beans.SearchData;
import com.glb.service.DataService;
import com.glb.service.TokenSericeCache;
import com.glb.utils.SecurityConstants;

@RestController
@RequestMapping("/rest")
public class SpringBootAppWS {
	
	@Autowired
	DataService dataService;
	
	@Autowired
	TokenSericeCache tokenSericeCache;

//	@Autowired
//	JobLauncher jobLauncher;
//
//	@Autowired
//	Job processJob;
//
	/*@RequestMapping(path = "/startJob", method = RequestMethod.GET, headers = "Accept=application/json", produces = "application/json")
	public String startJob() throws Exception {

		JobParameters jobParameters = new JobParametersBuilder().addLong(
				"time", System.currentTimeMillis()).toJobParameters();
		jobLauncher.run(processJob, jobParameters);

		return "Batch job has been invoked";
	}*/
	
	// inject via application.properties
		@Value("${welcome.message:test}")
		private String message = "Hello World";

		@RequestMapping("/")
		public String welcome(Map<String, Object> model) {
			model.put("message", this.message);
			return "welcome";
		}
	
	@RequestMapping(path =  "/userPage" , method = RequestMethod.GET, headers = "Accept=application/json", produces = "application/json")
    public  List<Data> getEmployees()
    {
        List<Data> employeesList = new ArrayList<>();
        return employeesList;
    }
	@RequestMapping(path =  "/homePage" , method = RequestMethod.GET, headers = "Accept=application/json", produces = "application/json")
    public  List<Data> getHomePage()
    {
        List<Data> employeesList = new ArrayList<>();
        return employeesList;
    }
	@RequestMapping(path =  "/adminPage" , method = RequestMethod.GET, headers = "Accept=application/json", produces = "application/json")
    public  List<Data> getAdminPage()
    {
        List<Data> employeesList = new ArrayList<>();
        return employeesList;
    }
	
	@RequestMapping(path =  "/getData" , method = RequestMethod.POST, headers = "Accept=application/json", produces = "application/json")
    public  GenericV<List<DataV>> getData(@RequestBody SearchData searchData)
    {
		GenericV< List<DataV>> genericV =new GenericV<>();
		List<DataV> data = dataService.getData(searchData);
		System.out.println(data);
		genericV.setObjects(data);
		genericV.setPage(searchData.getPageNumber());
		genericV.setTotalRec(SecurityConstants.TOTAL_RECORDS);
        return genericV;
    }
	
	@PostMapping("/status")
    public boolean status(@RequestBody String userName) {
		System.out.println("inside status::"+userName);
		if (null == tokenSericeCache.checkUserToken(userName)){
			return false;
		}
		System.out.println("out status::"+userName);
		return true;
    }

}