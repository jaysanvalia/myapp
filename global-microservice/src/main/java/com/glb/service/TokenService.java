package com.glb.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;

@Service
public class TokenService {

	private static Logger log = LoggerFactory.getLogger(TokenService.class);
	
	@Autowired
	Cache cache;

	public boolean clearCache(String user) {
		log.info("inside clearCache");
		return cache.remove(user);
		
	}

	public boolean cacheUser(String user, String token) {
		log.info("inside cacheUser");
		cache.put(new Element(user, token));
		return true;
	}
	
	public Element getCacheUser(String user) {
		log.info("inside getCacheUser");
		return cache.get(user);
	}
	
}