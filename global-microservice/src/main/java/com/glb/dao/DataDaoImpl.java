package com.glb.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.glb.beans.Data;
import com.glb.beans.SearchData;
import com.glb.utils.SecurityConstants;

import common.GenericCRUDDAOImpl;

@Repository
public class DataDaoImpl extends GenericCRUDDAOImpl<Data, Data> implements
		DataDAO {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<Data> getData(SearchData searchData) {
		List<Data> list = new ArrayList<>();
		list.add(new Data());
		/*int maxRecord=2;
		System.out.println(searchData);
		Session session = getSession();
		Criteria criteria = session.createCriteria(Data.class);
		criteria.add(Restrictions.eq("filedata.fileId", Integer.parseInt(searchData.getFileId())));

		SecurityConstants.TOTAL_RECORDS=(Long) criteria.setProjection(Projections.rowCount()).uniqueResult();
		
		criteria = session.createCriteria(Data.class);
		criteria.add(Restrictions.eq("filedata.fileId", Integer.parseInt(searchData.getFileId())));
		
		if (null != searchData.getAmount() && searchData.getAmount()!="") {
			criteria.add(Restrictions.eq("amount", Integer.parseInt(searchData.getAmount())));
		}
		if (null != searchData.getDate()) {
			criteria.add(Restrictions.eq("date", searchData.getDate()));
		}
		if (null != searchData.getName() && searchData.getName()!="") {
			criteria.add(Restrictions.eq("name", searchData.getName()));
		}
		
		criteria.setFirstResult((searchData.getPageNumber()-1)*maxRecord);
		criteria.setMaxResults(maxRecord);
		criteria.addOrder(Order.asc("id"));
		List list = criteria.list();*/
		return list ;
	}
}
