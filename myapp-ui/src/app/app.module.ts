import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { AuthService } from './services/auth.service';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { RegisterComponent } from './components/register/register.component';
import { StatusComponent } from './components/status/status.component';
import { EnsureAuthenticated } from './services/ensure-authenticated.service';
import { LoginRedirect } from './services/login-redirect.service';
import { LogoutComponent } from './components/logout/logout.component';
import { MainpageComponent } from './components/mainpage/mainpage.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import {NgxPaginationModule} from 'ngx-pagination'; 
import { CommonModule } from '@angular/common';
import { GlobalDataService } from './services/global-data.service';
import { LoginActivateService } from './services/login-activate.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    StatusComponent,
    LogoutComponent,
    MainpageComponent,
  ],
  imports: [
    BrowserModule,
	HttpModule,
	CommonModule,
	NgxPaginationModule,
	FormsModule,
	RouterModule.forRoot([{
	   path: '',
	   component: LoginComponent
	},
      { path: 'login', component: LoginComponent },
	 { path: 'register', component: RegisterComponent },
	{ path: 'status', component: StatusComponent ,canActivate:[LoginActivateService]},
	{ path: 'logout', component: LogoutComponent },
	{ path: 'main', component: MainpageComponent ,canActivate:[LoginActivateService]}
	])
  ],
  providers: [ GlobalDataService,AuthService,EnsureAuthenticated,LoginRedirect,LoginActivateService],
  bootstrap: [AppComponent]
})
export class AppModule { }
//{ provide: LocationStrategy, useClass: HashLocationStrategy },