package com.glb.processor;

import org.springframework.batch.core.annotation.OnSkipInProcess;
import org.springframework.batch.core.annotation.OnSkipInRead;
import org.springframework.batch.core.annotation.OnSkipInWrite;
import org.springframework.stereotype.Component;

@Component
public class CustomSkipListener {

    @OnSkipInRead
    public void onSkipInRead(Throwable t) {
        System.out.println("From onSkipInRead -> " + t.getMessage());
    }

    @OnSkipInWrite
    public void onSkipInWrite(String item, Throwable t) {
        System.out.println("From onSkipInWrite: " + item + " -> " + t.getMessage());
    }

    @OnSkipInProcess
    public void onSkipInProcess(String item, Throwable t) {
        System.out.println("From onSkipInProcess: " + item + " -> " + t.getMessage());
    }
}