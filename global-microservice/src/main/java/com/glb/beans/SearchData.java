package com.glb.beans;

public class SearchData {

	String amount;
	String name;
	String date;
	Integer pageNumber;
	String fileId;
	
	public String getFileId() {
		return fileId;
	}
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	public Integer getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	@Override
	public String toString() {
		return "SearchData [amount=" + amount + ", name=" + name + ", date="
				+ date + ", pageNumber=" + pageNumber + ", fileId=" + fileId
				+ "]";
	}
	
}
